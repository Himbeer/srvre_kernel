// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const std = @import("std");
const debug_console = @import("sbi/debug_console.zig");
const legacy = @import("sbi/legacy.zig");

provider: Provider,

const Self = @This();

pub const Provider = union(enum) {
    sbi_debug: debug_console.Writer,
    sbi_legacy: legacy.Writer,
};

pub fn autoChoose() ?Self {
    if (debug_console.writer()) |sbi_con| {
        return .{
            .provider = .{ .sbi_debug = sbi_con },
        };
    } else |_| {}
    if (legacy.writer()) |sbi_legacy_con| {
        return .{
            .provider = .{ .sbi_legacy = sbi_legacy_con },
        };
    } else |_| {}

    return null;
}

pub fn writer(console: *const Self) std.io.AnyWriter {
    switch (console.provider) {
        .sbi_debug => return console.provider.sbi_debug.any(),
        .sbi_legacy => return console.provider.sbi_legacy.any(),
    }
}
