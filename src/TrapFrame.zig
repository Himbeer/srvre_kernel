// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

general_purpose_registers: [32]usize, // Offset: 0
floating_point_registers: [32]usize, // Offset: 256
satp: usize, // Offset: 512
stack_pointer: *allowzero u8, // Offset: 520
hart_id: usize, // Offset: 528

const Self = @This();

pub fn setReturnValue(self: *Self, value: anytype) void {
    switch (@typeInfo(@TypeOf(value))) {
        .ErrorUnion => self.returnErrorUnion(value),
        .ErrorSet => self.returnError(value),
        else => self.returnValue(value),
    }
}

fn returnErrorUnion(self: *Self, error_union: anytype) void {
    if (error_union) |value| {
        self.returnValue(value);
    } else |err| {
        self.returnError(err);
    }
}

fn returnError(self: *Self, err: anyerror) void {
    self.general_purpose_registers[11] = @intFromError(err);
}

fn returnValue(self: *Self, value: anytype) void {
    self.general_purpose_registers[11] = 0;
    if (@typeInfo(@TypeOf(value)) != .Void) {
        self.general_purpose_registers[10] = @bitCast(value);
    }
}
