// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const std = @import("std");
const hwinfo = @import("../hwinfo.zig");
const riscv = @import("../riscv.zig");
const sbi = @import("../sbi.zig");

const ExtId: usize = 0x54494d45;

const FnId = enum(usize) {
    SetTimer,
};

pub const Error = error{
    NoCpusHwInfo,
};

pub fn setTimer(stime_absolute: u64) !void {
    if (!try sbi.probeExt(ExtId)) return sbi.Error.NotSupported;

    const ret = riscv.ecall(ExtId, @intFromEnum(FnId.SetTimer), stime_absolute, 0, 0);
    if (ret.err != 0) return sbi.errorFromCode(ret.err);
}

pub fn interruptInMillis(millis: u64) !void {
    var cpus = try hwinfo.byKind(.cpus);
    const frequency = cpus.next() orelse return error.NoCpusHwInfo;
    const cycles = frequency.value / 1000 * millis;

    const time = riscv.time.read();
    try setTimer(time + cycles);
}
