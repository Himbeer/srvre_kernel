// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const riscv = @import("../riscv.zig");
const sbi = @import("../sbi.zig");

const ExtId: usize = 0x53525354;

const FnId = enum(usize) {
    Reset,
};

pub const Type = enum(u32) {
    Shutdown,
    ColdReboot,
    WarmReboot,
};

pub const Reason = enum(u32) {
    None,
    SysErr,
};

pub fn reset(@"type": Type, reason: Reason) !void {
    if (!try sbi.probeExt(ExtId)) {
        return sbi.Error.NotSupported;
    }

    const type_id = @intFromEnum(@"type");
    const reason_id = @intFromEnum(reason);

    const ret = riscv.ecall(ExtId, @intFromEnum(FnId.Reset), type_id, reason_id, 0);
    if (ret.err != 0) {
        return sbierr.errorFromCode(ret.err);
    }
}
