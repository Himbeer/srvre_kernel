// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const std = @import("std");
const riscv = @import("../riscv.zig");
const sbi = @import("../sbi.zig");

const ExtId: usize = 0x4442434E;

const FnId = enum(usize) {
    Write,
    Read,
    WriteByte,
};

pub const Writer = std.io.Writer(void, sbi.Error, write);

fn write(_: void, bytes: []const u8) !usize {
    const ret = riscv.ecall(ExtId, @intFromEnum(FnId.Write), bytes.len, @intFromPtr(bytes.ptr), 0);
    if (ret.err != 0) {
        return sbi.errorFromCode(ret.err);
    }

    return @intCast(ret.val);
}

pub fn writer() !Writer {
    if (!try sbi.probeExt(ExtId)) {
        return sbi.Error.NotSupported;
    }

    return .{ .context = {} };
}
