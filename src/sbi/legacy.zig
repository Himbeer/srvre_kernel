// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const std = @import("std");
const riscv = @import("../riscv.zig");
const sbi = @import("../sbi.zig");

const ExtId = enum(usize) {
    SetTimer,
    ConsolePutchar,
    ConsoleGetchar,
    ClearIpi,
    SendIpi,
    RemoteFenceI,
    RemoteSFenceVma,
    RemoteSFenceVmaAsid,
    Shutdown,
};

pub const Writer = std.io.Writer(void, sbi.Error, write);

fn write(_: void, bytes: []const u8) !usize {
    for (bytes) |byte| {
        const ret = riscv.ecall(@intFromEnum(ExtId.ConsolePutchar), 0, byte, 0, 0);
        if (ret.err != 0) {
            return sbi.errorFromCode(ret.err);
        }
    }

    return bytes.len;
}

pub fn writer() !Writer {
    if (!try sbi.probeExt(@intFromEnum(ExtId.ConsolePutchar))) {
        return sbi.Error.NotSupported;
    }

    return .{ .context = {} };
}
