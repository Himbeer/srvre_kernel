// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const builtin = @import("builtin");
const std = @import("std");
const hwinfo = @import("hwinfo.zig");

pub fn main() !void {
    const stdin = std.io.getStdIn();
    const stdout = std.io.getStdOut();

    const reader = stdin.reader();
    const writer = stdout.writer();

    try writer.writeByte(@intFromBool(builtin.target.cpu.arch.endian() == .big));

    var buf = [_]u8{0} ** 256;
    while (try reader.readUntilDelimiterOrEof(&buf, '\n')) |line| {
        const dev = try hwinfo.Dev.parse(line);
        try writer.writeStruct(dev);
    }
}
