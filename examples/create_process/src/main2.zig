// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const srvre_sys = @import("srvre_sys");
const os = srvre_sys.os;

usingnamespace srvre_sys;

pub fn main() void {
    _ = os.consoleWrite("Hello from program 2\r\n") catch unreachable;
}
