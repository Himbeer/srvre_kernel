// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const srvre_sys = @import("srvre_sys");
const os = srvre_sys.os;

usingnamespace srvre_sys;

const program2 = @embedFile("program2");
var buffer: [program2.len]u8 = undefined;

pub fn main() void {
    _ = os.consoleWrite("Hello from program 1\r\n") catch unreachable;
    @memcpy(buffer[0..], program2);
    _ = os.launch(@alignCast(buffer[0..])) catch unreachable;
}
