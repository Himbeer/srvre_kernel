// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const srvre_sys = @import("srvre_sys");
const os = srvre_sys.os;

usingnamespace srvre_sys;

const program2 = @embedFile("program2");
var program2_aligned: [program2.len]u8 = undefined;

pub fn main() void {
    _ = os.consoleWrite("Hello from program 1\r\n") catch unreachable;

    os.join(0) catch unreachable;

    @memcpy(program2_aligned[0..], program2);
    _ = os.launch(@alignCast(program2_aligned[0..])) catch unreachable;

    var sender: u16 = undefined;
    var buffer: [256]u8 = undefined;
    while (true) {
        const n = os.receive(0, &sender, &buffer) catch continue;
        const message = buffer[0..n];

        _ = os.consoleWrite("Program 1 received message from PID ") catch unreachable;
        _ = os.consoleWrite(&[_]u8{'0' + @as(u8, @intCast(sender))}) catch unreachable;
        _ = os.consoleWrite(": ") catch unreachable;
        _ = os.consoleWrite(message) catch unreachable;
        _ = os.consoleWrite("\r\n") catch unreachable;
    }
}
