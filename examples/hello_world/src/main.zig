// SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

const srvre_sys = @import("srvre_sys");
const os = srvre_sys.os;

usingnamespace srvre_sys;

pub fn main() void {
    const s = "Hello from U-mode init\r\n";
    _ = os.consoleWrite(s) catch unreachable;
}
