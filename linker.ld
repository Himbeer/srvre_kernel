/*
 * SPDX-FileCopyrightText: 2024 Himbeer <himbeer@disroot.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

OUTPUT_ARCH("riscv")

MEMORY {
	ram (wxa) : ORIGIN = 0x80200000, LENGTH = 512M
}

PHDRS {
	lo_rx PT_LOAD FLAGS (5); /* R-X */
	lo_r  PT_LOAD FLAGS (4); /* R-- */
	lo_rw PT_LOAD FLAGS (6); /* RW- */
}

SECTIONS {
	. = 0x80200000;

	.text (0x80200000) : ALIGN(4K) {
		PROVIDE(_text_start = .);

		*(.text.start)
		*(.text .text.*)

		PROVIDE(_text_end = .);
	} > ram AT > ram : lo_rx

	PROVIDE(_global_pointer = .);

	.rodata            : ALIGN(4K) {
		PROVIDE(_rodata_start = .);

		*(.rodata .rodata.*)
		*(.srodata .srodata.*)

		PROVIDE(_rodata_end = .);
	} > ram AT > ram : lo_r
	.data              : ALIGN(4K) {
		PROVIDE(_data_start = .);

		*(.data .data.*)
		*(.sdata .sdata.*)

		PROVIDE(_data_end = .);
	} > ram AT > ram : lo_rw
	.bss               : ALIGN(4K) {
		PROVIDE(_bss_start = .);

		*(.bss .bss.*)
		*(.sbss .sbss.*)

		PROVIDE(_bss_end = .);
	} > ram AT > ram : lo_rw

	/DISCARD/ : { *(.note.GNU-stack) *(.gnu_debuglink) *(.gnu.lto_*) }

	PROVIDE(_memory_start = ORIGIN(ram));
	PROVIDE(_memory_end = ORIGIN(ram) + LENGTH(ram));

	. = ALIGN(4K);

	PROVIDE(_stack_start = .);
	. = _stack_start + 0x80000;
	PROVIDE(_stack_end = .);

	. = ALIGN(4K);

	PROVIDE(_stvec_stack_start = .);
	. = _stvec_stack_start + 0x1000;
	PROVIDE(_stvec_stack_end = .);

	PROVIDE(_heap_start = _stvec_stack_end);
	PROVIDE(_heap_end = _memory_end);
}
